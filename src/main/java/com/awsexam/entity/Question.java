package com.awsexam.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
 
@Entity
@Table(name = "QUESTIONS")
public class Question implements Serializable {
 
   	private static final long serialVersionUID = -5925792767165839407L;
   	
	private int quenumber;
	private String que;
	private String ans;
   
    @Id
    @Column(name = "quenumber", nullable = false)
    public int getQueNumber() {
		return quenumber;
	}

	public void setQueNumber(int quenumber) {
		this.quenumber = quenumber;
	}
    
	@Column(name = "que", nullable = false,length = 500)
	public String getQue() {
		return que;
	}

	public void setQue(String que) {
		this.que = que;
	}

	@Column(name = "ans", nullable = false)
	public String getAns() {
		return ans;
	}

	public void setAns(String ans) {
		this.ans = ans;
	}
}
