package com.awsexam.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
 
@Entity
@Table(name = "QUESTION_OPTIONS")
public class QuestionsOptions implements Serializable {
 
	private static final long serialVersionUID = -8205933648681593548L;
	
	private int opstionId;
	private Question queNumber;
    private String  code;
    private String  optionTxt;
 
    @Id
    @Column(name = "opstionid")    
    public int getOpstionId() {
		return opstionId;
	}

	public void setOpstionId(int opstionId) {
		this.opstionId = opstionId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quenumber", nullable = false,foreignKey = @ForeignKey(name = "QUESTIONS_FK") )
    public Question getQueNumber() {
		return queNumber;
	}

	public void setQueNumber(Question queNumber) {
		this.queNumber = queNumber;
	}

	@Column(name = "code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
    @Column(name = "optiontxt" , length = 500)
	public String getOptionTxt() {
		return optionTxt;
	}

	public void setOptionTxt(String optionTxt) {
		this.optionTxt = optionTxt;
	}

}