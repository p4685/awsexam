package com.awsexam.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
 
@Entity
@Table(name = "examstatus")
@IdClass(ExamId.class)
public class ExamStatus implements Serializable {
 
   	private static final long serialVersionUID = 8757464041419967669L;
   	
	private String examcode;
	private String username;
	private int questionsnum;
	private String selectedoptions;
	private boolean matched;
	private int marks;
	
	@Id 
    @Column(name = "examcode", nullable = false)
    public String getExamcode() {
		return examcode;
	}
	
	@Id 
	@Column(name = "username")
	public String getUsername() {
		return username;
	}

	@Id 
    @Column(name = "questionsnum")
	public int getQuestionsnum() {
		return questionsnum;
	}
    
    @Column(name = "selectedoptions")
	public String getSelectedoptions() {
		return selectedoptions;
	}

    @Column(name = "matched")
	public boolean isMatched() {
		return matched;
	}

    @Column(name = "marks")
	public int getMarks() {
		return marks;
	}

    public void setExamcode(String examcode) {
		this.examcode = examcode;
	}
    
	public void setUsername(String username) {
		this.username = username;
	}

	public void setQuestionsnum(int questionsnum) {
		this.questionsnum = questionsnum;
	}

	public void setSelectedoptions(String selectedoptions) {
		this.selectedoptions = selectedoptions;
	}

	public void setMatched(boolean matched) {
		this.matched = matched;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}
	
}
