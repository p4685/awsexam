package com.awsexam.entity;

import java.io.Serializable;

import javax.persistence.Id;

public class ExamId implements Serializable {

	private static final long serialVersionUID = -8787638110578071817L;
	
	@Id 
    private String examcode;
    @Id 
    private String username;
    @Id 
    private int questionsnum;
    
	public String getExamcode() {
		return examcode;
	}

	public void setExamcode(String examcode) {
		this.examcode = examcode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getQuestionsnum() {
		return questionsnum;
	}

	public void setQuestionsnum(int questionsnum) {
		this.questionsnum = questionsnum;	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + questionsnum;
        
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) return true;
        if (obj == null) return false;

        if (getClass() != obj.getClass()) return false;

        ExamId other = (ExamId) obj;

        if (examcode != other.examcode) return false;
        if (username != other.username) return false;
        if (questionsnum != other.questionsnum) return false;

        return true;
    }
}
