package com.awsexam.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "awsexams")
public class AwsExam {

	private int id;
	private String code;
	private String name;
	private int qstart;
	private int qend;
	private int passing;
	
	@Id 
	@Column(name = "Id")
	public int getId() {
		return id;
	}
	
	@Column(name = "code")
	public String getCode() {
		return code;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}
	
	@Column(name = "qstart")
	public int getQstart() {
		return qstart;
	}
	
	@Column(name = "qend")
	public int getQend() {
		return qend;
	}
	
	@Column(name = "passing")
	public int getPassing() {
		return passing;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setQstart(int qstart) {
		this.qstart = qstart;
	}

	public void setQend(int qend) {
		this.qend = qend;
	}

	public void setPassing(int passing) {
		this.passing = passing;
	}
	
		
}
