package com.awsexam.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
 
@Entity
@Table(name = "question_notes")
public class QuestionsNotes implements Serializable {
 
	private static final long serialVersionUID = -1708175831366226846L;
	
	private int id;
	private Question queNumber;
    private String  notes;
    
    @Id
    @Column(name = "id")
    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quenumber", nullable = false,foreignKey = @ForeignKey(name = "QUESTIONS_NOTES_FK") )
	public Question getQueNumber() {
		return queNumber;
	}

	public void setQueNumber(Question queNumber) {
		this.queNumber = queNumber;
	}

	@Column(name = "notes",length = 1000)
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}