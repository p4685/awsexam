package com.awsexam.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;

import com.awsexam.model.ExamInfo;
import com.awsexam.model.ExamStatusInfo;
import com.awsexam.security.PariwarUserDetails;
 
public class Utils {
 
 
    public static void removeCartInSession(HttpServletRequest request) {
        request.getSession().removeAttribute("myCart");
    }
 
    public static String getloggedInUserName() {
    	  Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	  String name = auth.getName();
    	  
    	  Object userDetails = SecurityContextHolder.getContext().getAuthentication().getDetails();
    	  if (userDetails instanceof PariwarUserDetails) {
    	      return ((PariwarUserDetails) userDetails).getFullUsername();
    	  }
    	  
    	  return name;
    }
    
    public static ExamStatusInfo getExamStatusInSession(HttpServletRequest request) {
        // Get Cart from Session.
    	String examCode= (String)request.getSession().getAttribute("examcode");
    	
    	System.out.println(">>>>>>>> Exam Code >>>>>>>>> "+examCode);
    	
    	ExamStatusInfo examStatus = (ExamStatusInfo) request.getSession().getAttribute(examCode+"Data");
        // If null, create it.
        if (examStatus == null) {
        	System.out.println(">>>>>>>> Creating New >>>>>>>>> "+examCode);
        	examStatus = new ExamStatusInfo();
        	HashMap<String,ExamInfo> examInfo=new LinkedHashMap<String,ExamInfo>();
        	examStatus.setExamInfo(examInfo);
            request.getSession().setAttribute(examCode+"Data", examStatus);
        }
        
        return examStatus;
    }
    
    public static String getOptionAsString(List<String> optionLst) {
    	String options="";
    	if(options!=null) {
			for(int i=0; i<optionLst.size(); i++){
				options+=StringUtils.trimWhitespace(optionLst.get(i));
			}
		}
    	return options;
    }
    
    public static List<String> convertStringToCharList(String str){
  
        // Create an empty List of character
        List<String> chars = new ArrayList<String>();
  
        // For each character in the String
        // add it to the List
        for (char ch : str.toCharArray()) {
            chars.add(Character.toString(ch));
        }
        
        // return the List
        return chars;
    }
    
}
