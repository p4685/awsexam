package com.awsexam.utility;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import com.awsexam.model.OptionsInfo;
import com.awsexam.model.OuestionsInfo;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSmartCopy;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;


public class ExamToPdfUtil {
	
	public static void createDocument(List<OuestionsInfo> qList,String dest) throws FileNotFoundException, DocumentException {
    	
    	// 1. Create document
        Document document = new Document(PageSize.A4, 10, 10, 10, 10);

        // 2. Create PdfWriter
        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(dest));

        // 3. Open document
        document.open();

        //add header
        addHeader(pdfWriter);
        addQuestionDetails(pdfWriter,qList);
        
        // 5. Close document
        document.close();
    }
         
	
    private static void addQuestionDetails(PdfWriter writer,List<OuestionsInfo> qList){
    	
    	PdfPTable cartHeader = new PdfPTable(1);
    	PdfPTable cart = new PdfPTable(2);
        
        try {
        	
        	cartHeader.setWidths(new int[]{100});
	       	cartHeader.setTotalWidth(130);
	       	cartHeader.setLockedWidth(true);
	       	cartHeader.getDefaultCell().setFixedHeight(40);
	       	cartHeader.getDefaultCell().setBorder(Rectangle.BOTTOM);
	       	cartHeader.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);
        	
	       	PdfPCell textNumber = new PdfPCell();
	       	textNumber.setPaddingBottom(5);
	       	textNumber.setPaddingLeft(5);
	       	textNumber.setBorder(Rectangle.BOTTOM);
	       	textNumber.setBorderColor(BaseColor.LIGHT_GRAY);
            
	       	textNumber.addElement(new Phrase("Question : ", new Font(Font.FontFamily.HELVETICA, 12)));
	       	cartHeader.addCell(textNumber);
	       
	       	addTableHeader(cart);
	       	
	       	writer.add(cartHeader);
	       	writer.add(cart);
	       	
	       	addQuestiondetails(qList,writer);
            // write content
	       	//cartHeader.writeSelectedRows(0, -1, 34, 720, writer.getDirectContent());
	        // write content
            //cart.writeSelectedRows(0, -1, 34, 680, writer.getDirectContent());
            
        } catch(DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
    
    private static void addQuestiondetails(List<OuestionsInfo> qList,PdfWriter writer) {
    		
    	qList.forEach((OuestionsInfo info)->{
    		
    		PdfPTable qTable = new PdfPTable(2);
    		
    		info.getQue();
    		qTable.addCell(getCartCell(String.valueOf(info.getQueNumber()),0));
    		qTable.addCell(getCartCell(info.getQue(),0));
    		
    		List<OptionsInfo> list = info.getOptions();
    		
    		list.forEach((OptionsInfo option)->{
    			qTable.addCell(getCartCell(String.valueOf(option.getCode()),0));
    			qTable.addCell(getCartCell(option.getOptionTxt(),0));
    		});
    		
    		try {
				writer.add(qTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        });
    }
    
    private static void addTableHeader(PdfPTable pTable) {
    	
    	try {
    		
	    	// set defaults
	    	pTable.setWidths(new int[]{50,500});
	    	pTable.setTotalWidth(550);
	    	pTable.setLockedWidth(true);
	    	pTable.getDefaultCell().setFixedHeight(30);
	    	pTable.getDefaultCell().setBorder(Rectangle.BOX);
	    	pTable.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);
	    	pTable.getDefaultCell().setBackgroundColor(BaseColor.BLUE); 
	    	
	    	addTableCell(pTable,"Question Number");
	    	addTableCell(pTable,"Question");
	    	
    	
	    } catch(DocumentException de) {
	        throw new ExceptionConverter(de);
	    }
   }
    
       
   private static PdfPCell getCartCell(String value,int colspan) {
	   
	   PdfPCell head1 = new PdfPCell();
       head1.setPaddingBottom(5);
       head1.setPaddingLeft(15);
       head1.setBorder(Rectangle.BOX);
       head1.setBorderColor(BaseColor.LIGHT_GRAY);
       head1.setHorizontalAlignment(Element.ALIGN_RIGHT);
       //head1.setBackgroundColor(new CMYKColor(72, 33, 0, 28)); 
       head1.setColspan(colspan);
              
       head1.addElement(new Phrase(value, new Font(Font.FontFamily.HELVETICA, 8)));
       //pTable.addCell(head1);
       
       return head1;
   }
    
    private static void addTableCell(PdfPTable pTable,String name) {
        // add text
       PdfPCell head1 = new PdfPCell();
       head1.setPaddingBottom(5);
       head1.setPaddingLeft(5);
       head1.setBorder(Rectangle.BOX);
       head1.setBorderColor(BaseColor.LIGHT_GRAY);
       head1.setHorizontalAlignment(Element.ALIGN_CENTER);
       head1.setBackgroundColor(new CMYKColor(72, 33, 0, 28)); 
              
       head1.addElement(new Phrase(name, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD)));
       pTable.addCell(head1);
    }
	
    private static void addHeader(PdfWriter writer){
        PdfPTable header = new PdfPTable(2);
        try {
            // set defaults
            header.setWidths(new int[]{50, 50});
            header.setTotalWidth(750);
            header.setLockedWidth(true);
            header.getDefaultCell().setFixedHeight(40);
            header.getDefaultCell().setBorder(Rectangle.BOTTOM);
            header.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);
            // add image
            
            Image logo = Image.getInstance("src/main/resources/static/images/cloud.png");
            header.addCell(logo);
            // write content
            header.writeSelectedRows(0, -1, 34, 803, writer.getDirectContent());
            
        } catch(DocumentException de) {
            throw new ExceptionConverter(de);
        } catch (MalformedURLException e) {
            throw new ExceptionConverter(e);
        } catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }

    private static void addFooter(PdfWriter writer){
        PdfPTable footer = new PdfPTable(3);
        try {
            // set defaults
            footer.setWidths(new int[]{24, 2, 1});
            footer.setTotalWidth(527);
            footer.setLockedWidth(true);
            footer.getDefaultCell().setFixedHeight(40);
            footer.getDefaultCell().setBorder(Rectangle.TOP);
            footer.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);

            // add copyright
            footer.addCell(new Phrase("\u00A9 thincloud.in ", new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD)));
            
            // write page
            PdfContentByte canvas = writer.getDirectContent();
            footer.writeSelectedRows(0, -1, 34, 100, canvas);
                       
        } catch(DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
}
