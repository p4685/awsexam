package com.awsexam.utility;

import java.util.ArrayList;
import java.util.List;

public class Testing {

	ArrayList<Integer> navigationPages;
	private int currentPage=1;
	
    private int totalRecords;
    private int maxResult;
    private int totalPages=30;
    private int maxNavigationPage=30;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
				
		double per = (4/50.0)*100;
		
		 // Get the String to be converted
        //String str = "ABC";
  
        // Get the List of Character
        //List<String>  chars = convertStringToCharList(str);
  
        // Print the list of characters
        System.out.println(per);
	}
	
	
	public static List<String>
	
    convertStringToCharList(String str)
    {
  
        // Create an empty List of character
        List<String> chars = new ArrayList<>();
  
        // For each character in the String
        // add it to the List
        for (char ch : str.toCharArray()) {
            chars.add(Character.toString(ch));
        }
        
        // return the List
        return chars;
    }
	
	public void navigationPages() {
		
		navigationPages = new ArrayList<Integer>();
		 
	   int current = currentPage > this.totalPages ? this.totalPages : this.currentPage;
	   System.out.println("current ==> "+current);
	 
	   int begin = current;
	   int end = current + this.maxNavigationPage;
	   
	   System.out.println("begin ==> "+begin);
	   System.out.println("end ==> "+end);
	   
	// First page
       navigationPages.add(1);
       if (begin > 2) {
           // For '...'
           navigationPages.add(-1);
       }
 
       for (int i = begin; i < end; i++) {
           if (i > 1 && i < this.totalPages) {
               navigationPages.add(i);
           }
       }
 
       if (end < this.totalPages - 2) {
           // For '...'
           navigationPages.add(-1);
       }
       // Last page.
       navigationPages.add(this.totalPages);
	   
       System.out.println("navigationPages ==> "+navigationPages);
	   
	}
	

}
