package com.awsexam.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.awsexam.entity.AwsExam;
import com.awsexam.entity.ExamStatus;
import com.awsexam.entity.Question;
import com.awsexam.model.AwsExamInfo;
import com.awsexam.model.ExamInfo;
import com.awsexam.model.ExamStatusInfo;
import com.awsexam.model.OptionsInfo;
import com.awsexam.model.OuestionsInfo;
import com.awsexam.model.PaginationResult;
import com.awsexam.repository.AwsExamRepository;
import com.awsexam.repository.CLFC01NotesRepository;
import com.awsexam.repository.CLFC01OptionsRepository;
import com.awsexam.repository.CLFC01QuestionsRepository;
import com.awsexam.repository.ExamStatusRepository;

@Service
public class QuestionService {

	@Autowired
	private ExamStatusRepository examStatusRepository;
	@Autowired
	private CLFC01OptionsRepository optionsRepository;
	@Autowired
	private CLFC01QuestionsRepository questionsRepository;
	@Autowired
	private AwsExamRepository awsExamRepository;
	@Autowired
	private CLFC01NotesRepository notesRepository;
	
	int nextPage;
	
	Logger logger= LoggerFactory.getLogger(this.getClass());

	public Question findOrder(Integer _queNumber) {
		return questionsRepository.findQuestions(_queNumber);
	}
	
	public List<AwsExam> getAllAwsExam() {
		List<AwsExam> examLst =awsExamRepository.getAwsExams();
		
		
		return examLst;
    }
	
	public ExamStatus findExam(String username,String examcode,int questionsNum) {
        return examStatusRepository.getExamStatus(username, examcode , questionsNum);
    }
	
    public List<ExamStatus> getSavedQuestions(String username,String examcode) {
		List<ExamStatus>  questionList = examStatusRepository.getAllQuestion(username, examcode);
		return questionList;
	}
    
    public List<ExamStatus> getSavedQuestions(String username) {
		List<ExamStatus>  questionList = examStatusRepository.getAllQuestion(username);
		return questionList;
	}
    
    public HashMap<String,AwsExamInfo> getSavedQuestionsMap(String username) {
		List<ExamStatus>  dbQquestionList = examStatusRepository.getAllQuestion(username);
		HashMap<String,AwsExamInfo> qMap = new LinkedHashMap<String,AwsExamInfo>();
		
		dbQquestionList.forEach((ExamStatus stat) ->{
			if(qMap.containsKey(stat.getExamcode())){
				int tMarks = stat.getMarks()+qMap.get(stat.getExamcode()).getTotalMarks();
				double per = (tMarks/50.0)*100;
				int qAttended = qMap.get(stat.getExamcode()).getAttended();
				
				qMap.get(stat.getExamcode()).setTotalMarks(tMarks);
				qMap.get(stat.getExamcode()).setPercentage(per);
				qMap.get(stat.getExamcode()).setAttended(qAttended+1);
				
			}else {
				AwsExamInfo queInfo = new AwsExamInfo();
				queInfo.setCode(stat.getExamcode());
				queInfo.setStarted(true);
				queInfo.setTotalMarks(stat.getMarks());
				queInfo.setAttended(1);
				qMap.put(stat.getExamcode(), queInfo);
			}
        });
		
		return qMap;
	}
	
	
	public void saveExam(List<ExamStatus> qListForSave) {
		List<ExamStatus> q = examStatusRepository.saveAll(qListForSave);
		q.forEach((ExamStatus examStatus) -> {
			logger.info("\t-------"+examStatus.getQuestionsnum());
		});
	}
	
	public void updateExam(ExamStatus _examStatus) {
		examStatusRepository.updateExam(_examStatus.getUsername(), _examStatus.getExamcode(),
				_examStatus.getQuestionsnum(), _examStatus.getSelectedoptions(), _examStatus.isMatched(),
				_examStatus.getMarks());
	}

	public PaginationResult<OuestionsInfo> listAllOuestions(int page, int maxResult, int maxNavigationPage) {

		PageRequest pagerequest= PageRequest.of(page - 1, maxResult);
		Page<Question> questionPage = questionsRepository.findAll(pagerequest);

		List<Question> listQuestions = questionPage.getContent();
		List<OuestionsInfo> questionsInfoList = new ArrayList<OuestionsInfo>();

		listQuestions.forEach((Question ques) -> {
			OuestionsInfo queInfo = new OuestionsInfo(ques.getQueNumber(), ques.getQue(), ques.getAns());
			List<OptionsInfo> allAvailableOptions = optionsRepository.listOptions(ques.getQueNumber());
			String notes = notesRepository.findNotes(ques.getQueNumber());
			queInfo.setOptions(allAvailableOptions);
			queInfo.setNotes(notes);
			questionsInfoList.add(queInfo);
		});

		return new PaginationResult<OuestionsInfo>(questionsInfoList, page, maxResult, maxNavigationPage);
	}
	
	public PaginationResult<OuestionsInfo> listOuestions(int page, int maxResult, int maxNavigationPage,
			ExamStatusInfo examSessionState) {

		PageRequest pagerequest= PageRequest.of(page - 1, maxResult);
		Page<Question> questionPage = questionsRepository.findAll(pagerequest);

		List<Question> listQuestions = questionPage.getContent();
		List<OuestionsInfo> questionsInfoList = new ArrayList<OuestionsInfo>();

		listQuestions.forEach((Question ques) -> {
			OuestionsInfo queInfo = new OuestionsInfo(ques.getQueNumber(), ques.getQue(), ques.getAns());
			List<OptionsInfo> allAvailableOptions = optionsRepository.listOptions(ques.getQueNumber());
			String notes = notesRepository.findNotes(ques.getQueNumber());
			syncWithSessionState(allAvailableOptions, examSessionState.getExamInfo().get(String.valueOf(ques.getQueNumber())));
			queInfo.setOptions(allAvailableOptions);
			queInfo.setNotes(notes);
			questionsInfoList.add(queInfo);
		});

		return new PaginationResult<OuestionsInfo>(questionsInfoList, page, maxResult, maxNavigationPage);
	}

	public void syncWithSessionState(List<OptionsInfo> availableOptions, ExamInfo sessionExamInfo) {
		final Logger logger;
		logger = LoggerFactory.getLogger(this.getClass());
		if (sessionExamInfo != null) {
			List<String> selectedOptions = sessionExamInfo.getSelOptionsList();
			logger.info("selected options quenumber : " + sessionExamInfo.getQuestionsNum());
			availableOptions.forEach((OptionsInfo optionInfo) -> {
				if (selectedOptions.contains(StringUtils.trimWhitespace(optionInfo.getCode()))) {
					optionInfo.setChecked("checked");
					logger.info("\t\t options : " + optionInfo.getCode() + "selected :" + optionInfo.getChecked());
				}
			});
		}
	}
}
