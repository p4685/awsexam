package com.awsexam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.awsexam.entity.UserEntity;
import com.awsexam.exception.UserNotFoundException;
import com.awsexam.model.User;
import com.awsexam.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;

	public User getUserDetails(String userId) throws UserNotFoundException{
		UserEntity ue = userRepository.getOne(userId);
		
		if (ue == null){			
			throw new UserNotFoundException(
					"UserService.USER_NOT_FOUND");
		}
		User user = new User();
		user.setCity(ue.getCity());
		user.setEmail(ue.getEmail());
		user.setName(ue.getName());
		user.setPhone(ue.getPhone());
		user.setUserId(ue.getUserId());
		
		return user;				
	}
}
