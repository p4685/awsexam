/**
 * 
 */
package com.awsexam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.awsexam.entity.UserEntity;
import com.awsexam.exception.InvalidCredentialException;
import com.awsexam.model.Login;
import com.awsexam.repository.UserRepository;

/**
 * The Class AadharService.
 */
@Service

public class LoginService {

	@Autowired
	private UserRepository userRepository;

	@Lazy
	@Autowired
    private PasswordEncoder passwordEncoder;
	
	public UserEntity authenticateLogin(Login userLogin) throws InvalidCredentialException {
		UserEntity user = userRepository.getOne(userLogin.getUserName());
		
		/*System.out.println("userLogin  ==> "+userLogin.getPassword());
		System.out.println("user  ==> "+user.getPassword());
		System.out.println(" 2 >> "+passwordEncoder.matches(userLogin.getPassword(), user.getPassword()));*/
		
		if (user == null) {
			throw new InvalidCredentialException("LoginService.INVALID_CREDENTIALS");
		} else if (!(passwordEncoder.matches(userLogin.getPassword(), user.getPassword()))) {
			throw new InvalidCredentialException("LoginService.INVALID_CREDENTIALS" + ".");
		}
		return user;
	}
}
