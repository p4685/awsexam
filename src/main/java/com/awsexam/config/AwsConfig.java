package com.awsexam.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
//import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;

@Configuration
public class AwsConfig {

	@Value("${spring.aws.accessId}")
    private String awsAccessKeyId;
 
    @Value("${spring.aws.secretId}")
    private String awsSecretKey;
 
    @Value("${spring.aws.region}")
    private String awsRegion;
    
    @Value("${spring.aws.bucketName}")
    private String bucketName;
    
    @Value("${spring.aws.pdfSourceLoc}")
    private String pdfSourceLoc;
    
    
    @Bean
    public AwsConfigUtil awsConfigUtil() {
    	
    	AwsConfigUtil util = AwsConfigUtil.create();
    	util.setAwsAccessKeyId(awsAccessKeyId);
    	util.setAwsRegion(awsRegion);
    	util.setAwsSecretKey(awsSecretKey);
    	util.setBucketName(bucketName);
    	util.setPdfSourceLoc(pdfSourceLoc);
    	
    	return util;
    }
    
   /* @Bean
    private AwsBasicCredentials awsBasicCredentials() {
        return AwsBasicCredentials.create(awsAccessKeyId, awsSecretKey);
    }
 
    @Bean
    public StaticCredentialsProvider staticCredentialsProvider() {
        return StaticCredentialsProvider.create(awsBasicCredentials());
    }*/
}
