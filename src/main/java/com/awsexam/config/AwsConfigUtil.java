package com.awsexam.config;

public final class AwsConfigUtil {
	
    private String awsAccessKeyId;
    private String awsSecretKey;
    private String awsRegion;
    private String bucketName;
    private String pdfSourceLoc;
	
    public static AwsConfigUtil create() {
        return new AwsConfigUtil();
    }

	public String getAwsAccessKeyId() {
		return awsAccessKeyId;
	}

	public String getAwsSecretKey() {
		return awsSecretKey;
	}

	public String getAwsRegion() {
		return awsRegion;
	}

	public String getBucketName() {
		return bucketName;
	}

	public String getPdfSourceLoc() {
		return pdfSourceLoc;
	}

	protected void setAwsAccessKeyId(String awsAccessKeyId) {
		this.awsAccessKeyId = awsAccessKeyId;
	}

	protected void setAwsSecretKey(String awsSecretKey) {
		this.awsSecretKey = awsSecretKey;
	}

	protected void setAwsRegion(String awsRegion) {
		this.awsRegion = awsRegion;
	}

	protected void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	protected void setPdfSourceLoc(String pdfSourceLoc) {
		this.pdfSourceLoc = pdfSourceLoc;
	}
	
}
