package com.awsexam.model;

public class NotesInfo {
 
    private int queNumber;
    private String notes;
      
    // Using for Hibernate Query.
    public NotesInfo(int _queNumber, String  _notes) {
        this.queNumber = _queNumber;
        this.notes = _notes;
    }

	public int getQueNumber() {
		return queNumber;
	}

	public String getNotes() {
		return notes;
	}

	protected void setQueNumber(int queNumber) {
		this.queNumber = queNumber;
	}

	protected void setNotes(String notes) {
		this.notes = notes;
	}
}
