package com.awsexam.model;

public class OptionsInfo {
 
    private int queNumber;
    private String code;
    private String optionTxt;
    private String checked="";
  
    // Using for Hibernate Query.
    public OptionsInfo(int _queNumber, String  _code, String _optionTxt) {
        this.queNumber = _queNumber;
        this.code = _code;
        this.optionTxt = _optionTxt;
    }

	public int getQueNumber() {
		return queNumber;
	}

	public String getCode() {
		return code;
	}

	public String getOptionTxt() {
		return optionTxt;
	}

	protected void setQueNumber(int queNumber) {
		this.queNumber = queNumber;
	}

	protected void setCode(String code) {
		this.code = code;
	}

	protected void setOptionTxt(String optionTxt) {
		this.optionTxt = optionTxt;
	}

	public String getChecked() {
		return checked;
	}

	public void setChecked(String checked) {
		this.checked = checked;
	}

}
