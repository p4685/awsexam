package com.awsexam.model;

import java.util.HashMap;

public class ExamStatusInfo {

	private String username;	
	private String examcode;	
	private HashMap<String,ExamInfo> examInfo;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getExamcode() {
		return examcode;
	}
	public void setExamcode(String examcode) {
		this.examcode = examcode;
	}
	public HashMap<String, ExamInfo> getExamInfo() {
		return examInfo;
	}
	public void setExamInfo(HashMap<String, ExamInfo> examInfo) {
		this.examInfo = examInfo;
	}
	
	
}
