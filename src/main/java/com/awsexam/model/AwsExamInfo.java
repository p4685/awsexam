package com.awsexam.model;

public class AwsExamInfo {

	private String code;
	private String name;
	private int qstart;
	private int qend;
	private boolean started=false;
	private int totalMarks;
	private String result;
	private double percentage;
	private int attended;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQstart() {
		return qstart;
	}
	public void setQstart(int qstart) {
		this.qstart = qstart;
	}
	public int getQend() {
		return qend;
	}
	public void setQend(int qend) {
		this.qend = qend;
	}
	public int getTotalMarks() {
		return totalMarks;
	}
	public void setTotalMarks(int totalMarks) {
		this.totalMarks = totalMarks;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public double getPercentage() {
		return percentage;
	}
	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
	public boolean isStarted() {
		return started;
	}
	public void setStarted(boolean started) {
		this.started = started;
	}
	public int getAttended() {
		return attended;
	}
	public void setAttended(int attended) {
		this.attended = attended;
	}
			
}
