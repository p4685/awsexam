package com.awsexam.model;

import java.util.ArrayList;
import java.util.List;

public class ExamInfo {
	
	private int questionsNum;	
	private String selOptions;	
	private List<String> selOptionsList= new ArrayList();
	private boolean matched=false;	
	private int marks=0;
	  
	  
	public int getQuestionsNum() {
		return questionsNum;
	}
	public void setQuestionsNum(int questionsNum) {
		this.questionsNum = questionsNum;
	}
	public String getSelOptions() {
		return selOptions;
	}
	public void setSelOptions(String selOptions) {
		this.selOptions = selOptions;
	}
	
	public int getMarks() {
		return marks;
	}
	public void setMarks(int marks) {
		this.marks = marks;
	}
	public List<String> getSelOptionsList() {
		return selOptionsList;
	}
	public void setSelOptionsList(List<String> selOptionsList) {
		this.selOptionsList = selOptionsList;
	}
	public boolean isMatched() {
		return matched;
	}
	public void setMatched(boolean matched) {
		this.matched = matched;
	}
	
}
