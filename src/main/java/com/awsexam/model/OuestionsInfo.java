package com.awsexam.model;

import java.util.List;
 
public class OuestionsInfo {
 
    private int queNumber;
    private String que;
    private String ans;
    private String notes;
  
    //QUESTIONS (queNumber,que,ans 
    private List<OptionsInfo> options;
 
    // Using for Hibernate Query.
    public OuestionsInfo(int _queNumber, String  _que, String _ans) {
        this.queNumber = _queNumber;
        this.que = _que;
        this.ans = _ans;
    }

	public int getQueNumber() {
		return queNumber;
	}

	public String getQue() {
		return que;
	}

	public String getAns() {
		return ans;
	}

	protected void setQueNumber(int queNumber) {
		this.queNumber = queNumber;
	}

	protected void setQue(String que) {
		this.que = que;
	}

	protected void setAns(String ans) {
		this.ans = ans;
	}

	public List<OptionsInfo> getOptions() {
		return options;
	}

	public void setOptions(List<OptionsInfo> options) {
		this.options = options;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
}
