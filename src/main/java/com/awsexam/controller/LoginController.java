package com.awsexam.controller;

import java.util.Enumeration;
import java.util.Optional;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.awsexam.entity.UserEntity;
import com.awsexam.model.Login;
import com.awsexam.security.SecurityService;
import com.awsexam.service.LoginService;
import com.awsexam.service.QuestionService;
import com.awsexam.utility.Utils;

@Controller
@SessionAttributes({"userName","userId"})
public class LoginController {
	
	@Lazy
	@Autowired
    private PasswordEncoder passwordEncoder;
	@Autowired
	private LoginService loginService;
	@Autowired
	private Environment environment;
	@Autowired
	private SecurityService securityService;
	@Autowired
	private QuestionService questionService;
	
	private String login="login";

	@GetMapping(value = "/")
	public ModelAndView gethome(@RequestParam Optional<String> error) {
		return new ModelAndView(login,"","");
	}
	
	@GetMapping(value = "/login")
	public ModelAndView getLoginDetails(@RequestParam Optional<String> error) {
		return new ModelAndView(login,"","");
	}

	@PostMapping(value = "/authenticateLogin")
	public String authenticateLogin(@Valid @ModelAttribute("command") Login userLogin, BindingResult result,
			ModelMap model, SessionStatus status,HttpSession session) {		

		//ModelAndView modelAndView = new ModelAndView("error"); 
		String view = "error";
		status.setComplete();
		
		try {
			if (result.hasErrors()) {
				view="login";
				model.addAttribute("command", userLogin);
				//modelAndView= new ModelAndView(login, "command", userLogin);
			}
			else{
				UserEntity userEntity = loginService.authenticateLogin(userLogin);
				
				securityService.autoLogin(userLogin.getUserName(), userLogin.getPassword());
				
				model.addAttribute("userName", userEntity.getName());
				final int maxResult = 5;
		        final int maxNavigationPage = 10;
		        /*PaginationResult<ProductInfo> productResult = productService.queryProducts(1, 
		                maxResult, maxNavigationPage, "");
		        model.addAttribute("paginationProducts", productResult);*/
		        model.addAttribute("userName", Utils.getloggedInUserName());
		        
		        view="redirect:/listExams";
                //modelAndView = new ModelAndView("listExams");	
			}
		}
		catch (Exception e) {
			if (e.getMessage().contains("LoginService")) {
				view="login";
				model.addAttribute("loginName",  userLogin.getUserName());
				//modelAndView = new ModelAndView(login); 
				//modelAndView.addObject("loginName", userLogin.getUserName());
			}
			//modelAndView.addObject("message", environment.getProperty(e.getMessage()));
			//model.addObject("message", environment.getProperty(e.getMessage()));
		}
		
		return view;
	}
	
	@GetMapping(value = "/logout")
	public ModelAndView logout(HttpSession session) {
		ModelAndView model = new ModelAndView("home", "", "");
		Enumeration<String> attributes = session.getAttributeNames();

		while (attributes.hasMoreElements())
			session.removeAttribute(attributes.nextElement());
		
		model.addObject("logoutMessage", environment.getProperty("LoginController.LOGOUT_SUCCESS"));
		return model;
	}

}
