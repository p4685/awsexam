package com.awsexam.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.awsexam.config.AwsConfigUtil;
import com.awsexam.entity.AwsExam;
import com.awsexam.entity.ExamStatus;
import com.awsexam.model.AwsExamInfo;
import com.awsexam.model.ExamInfo;
import com.awsexam.model.ExamStatusInfo;
import com.awsexam.model.OuestionsInfo;
import com.awsexam.model.PaginationResult;
import com.awsexam.service.QuestionService;
import com.awsexam.utility.ExamToPdfUtil;
import com.awsexam.utility.Utils;
import com.itextpdf.text.DocumentException;

@Controller
public class CLFC01Controller {
	
	@Autowired
	private QuestionService questionService;
	
	@Autowired
    private AwsConfigUtil awsConfigUtil;
	
	Logger logger= LoggerFactory.getLogger(this.getClass());
	
	@GetMapping(value = "/searchQuestion")
	public ModelAndView orderSearch(Model model,@RequestParam Optional<String> error) {
		model.addAttribute("userName", Utils.getloggedInUserName());
		return new ModelAndView("searchQuestion","","");
	}
	
	@GetMapping("/listExams")
    public String listExams(Model model, HttpSession session, HttpServletRequest request){
        
        List<AwsExam> qLst =questionService.getAllAwsExam();
        List<AwsExamInfo> finalList =new ArrayList<AwsExamInfo>();
        
        String username= Utils.getloggedInUserName();
        logger.info("-----logged in User ---- "+username);
        
        HashMap<String,AwsExamInfo> qMap = questionService.getSavedQuestionsMap(username);
        
        qLst.forEach((AwsExam exam)->{
        	AwsExamInfo eInfo= new AwsExamInfo();
        	
        	eInfo.setCode(exam.getCode());
        	eInfo.setName(exam.getName());
        	eInfo.setQstart(exam.getQstart());
        	eInfo.setQend(exam.getQend());
        	
        	if(qMap.containsKey(exam.getCode())) {
        		eInfo.setStarted(true);
            	eInfo.setTotalMarks(qMap.get(exam.getCode()).getTotalMarks());
            	eInfo.setPercentage(qMap.get(exam.getCode()).getPercentage());
            	eInfo.setAttended(qMap.get(exam.getCode()).getAttended());
            	
            	if(eInfo.getPercentage()>70) {
            		eInfo.setResult("Pass");
            	}else {
            		eInfo.setResult("Fail");
            	}
        	}else {
        	   eInfo.setStarted(false);
               eInfo.setTotalMarks(0);
               eInfo.setPercentage(0);
               eInfo.setAttended(0);
        	}
        	finalList.add(eInfo);
        });
        
        qMap.clear();
        qLst.clear();
        
        model.addAttribute("awsExamList", finalList);
        
        return "awsExamList";
    }
	
	@PostMapping("/navigateQuestion")
    public String nextQuestion(Model model, HttpSession session, HttpServletRequest request,
    		@RequestParam(value = "qCurrent", defaultValue = "1") String qCurrent) {
		
		int startQues=0;
		int endQues=1;
		int currentQues=0;
		
        final int MAX_RESULT = 1;
        final int MAX_NAVIGATION_PAGE = 50;
        
        try {
        	
        	startQues = Integer.parseInt(request.getParameter("qStart"));
        	endQues = Integer.parseInt(request.getParameter("qEnd"));
        	currentQues = Integer.parseInt(qCurrent);
        	
        	if(currentQues<startQues) currentQues=startQues;
            if(currentQues>endQues) currentQues=endQues;
            
        } catch (Exception e) {
        }
        
        model.addAttribute("userName", Utils.getloggedInUserName());
        ExamStatusInfo examStatusInfo = Utils.getExamStatusInSession(request);
        updateExamStausInSession(request);
 
		PaginationResult<OuestionsInfo> paginationResult = questionService.listOuestions(currentQues, MAX_RESULT,
				MAX_NAVIGATION_PAGE,examStatusInfo);
		
		model.addAttribute("startQues", startQues);
        model.addAttribute("endQues", endQues);
        model.addAttribute("currentQues", currentQues);
		model.addAttribute("paginationQuestionResult", paginationResult);
		model.addAttribute("qList", examStatusInfo.getExamInfo());
        
        return "questionsList";
	}
	
	@GetMapping("/getQuestions")
	public String getQuestions(Model model, HttpSession session, HttpServletRequest request,
    		@RequestParam(value = "qStart", defaultValue = "1") String qStart,
    		@RequestParam(value = "qCurrent", defaultValue = "1") String qCurrent,
    		@RequestParam(value = "qEnd", defaultValue = "1") String qEnd){
		
		//Store examcode in session
		String examcode=request.getParameter("examcode");
        request.getSession().setAttribute("examcode",examcode);
        String username= Utils.getloggedInUserName();
        
        logger.info("-----logged in User ---- "+username);
        
        //load saved details fromDB
        List<ExamStatus> qDBList= questionService.getSavedQuestions(username, examcode);
        
       //Sync DB details with session
        ExamStatusInfo sessionData = Utils.getExamStatusInSession(request);
        sessionData.setExamcode(examcode);
        sessionData.setUsername(username);
		
        logger.info("----- DB Questions for ------"+username+" examcode "+examcode);
        qDBList.forEach((ExamStatus dDBLst) -> {
        	
        	ExamInfo info = new ExamInfo();
        	info.setQuestionsNum(dDBLst.getQuestionsnum());
        	info.setSelOptions(dDBLst.getSelectedoptions());
        	info.setMarks(dDBLst.getMarks());
        	info.setSelOptionsList(Utils.convertStringToCharList(dDBLst.getSelectedoptions()));
        	info.setMatched(dDBLst.isMatched());
        	
        	sessionData.getExamInfo().put(String.valueOf(dDBLst.getQuestionsnum()), info);
			logger.info("\t------"+dDBLst.getQuestionsnum()+"  Options "+dDBLst.getSelectedoptions());
		});
        
        model.addAttribute("userName", Utils.getloggedInUserName());
        model.addAttribute("qList", sessionData.getExamInfo());
        
		return "redirect:/listQuestion?qStart="+qStart+"&qCurrent="+qCurrent+"&qEnd="+qEnd;
	}
	
	@GetMapping("/downloadAsPdf")
    public ResponseEntity<InputStreamReader> downloadAsPdf(Model model, HttpSession session, HttpServletRequest request,
    		@RequestParam(value = "qStart", defaultValue = "1") String qStart,
    		@RequestParam(value = "qCurrent", defaultValue = "1") String qCurrent,
    		@RequestParam(value = "qEnd", defaultValue = "1") String qEnd) throws FileNotFoundException, DocumentException{
		
		int startQues=0;
		int endQues=1;
		int currentQues=0;
		final int MAX_RESULT = 50;
        final int MAX_NAVIGATION_PAGE = 50;
        
        String examcode=request.getParameter("examcode");
        
        try {
        	startQues = Integer.parseInt(qStart);
        	endQues = Integer.parseInt(qEnd);
        	currentQues=Integer.parseInt(qCurrent);
        			
            if(currentQues<startQues) currentQues=startQues;
            if(currentQues>endQues) currentQues=endQues;
            
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
		PaginationResult<OuestionsInfo> paginationResult = questionService.listAllOuestions(currentQues, MAX_RESULT,
				MAX_NAVIGATION_PAGE);
                     
		String dest =awsConfigUtil.getPdfSourceLoc()+examcode+".pdf"; 
    	
		ExamToPdfUtil.createDocument(paginationResult.getList(),dest);
		
		File file2Upload = new File(dest);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        InputStreamReader i = new InputStreamReader(new FileInputStream(dest));
        
        System.out.println("The length of the file is : "+file2Upload.length());

        return ResponseEntity.ok().headers(headers).contentLength(file2Upload.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(i);
	}
	
	
	@GetMapping("/listQuestion")
    public String listQuestion(Model model, HttpSession session, HttpServletRequest request,
    		@RequestParam(value = "qStart", defaultValue = "1") String qStart,
    		@RequestParam(value = "qCurrent", defaultValue = "1") String qCurrent,
    		@RequestParam(value = "qEnd", defaultValue = "1") String qEnd){
    	
		int startQues=0;
		int endQues=1;
		int currentQues=0;
		final int MAX_RESULT = 1;
        final int MAX_NAVIGATION_PAGE = 50;
        
        try {
        	startQues = Integer.parseInt(qStart);
        	endQues = Integer.parseInt(qEnd);
        	currentQues=Integer.parseInt(qCurrent);
        			
            if(currentQues<startQues) currentQues=startQues;
            if(currentQues>endQues) currentQues=endQues;
            
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
        updateExamStausInSession(request);
        ExamStatusInfo examStatusInfo = Utils.getExamStatusInSession(request);
        
        
		PaginationResult<OuestionsInfo> paginationResult = questionService.listOuestions(currentQues, MAX_RESULT,
				MAX_NAVIGATION_PAGE, examStatusInfo);
                     
        model.addAttribute("startQues", startQues);
        model.addAttribute("endQues", endQues);
        model.addAttribute("currentQues", currentQues);
        model.addAttribute("paginationQuestionResult", paginationResult);
        model.addAttribute("qList", examStatusInfo.getExamInfo());
        
        return "questionsList";
    }
	
	
	@PostMapping("/saveExam")
    public String saveExam(Model model, HttpSession session, HttpServletRequest request,
    		@RequestParam(value = "qCurrent", defaultValue = "1") String qCurrent) {
		
		int startQues=0;
		int endQues=1;
		int currentQues=0;
        final int MAX_RESULT = 1;
        final int MAX_NAVIGATION_PAGE = 30;
        
        try {
        	startQues = Integer.parseInt(request.getParameter("qStart"));
        	endQues = Integer.parseInt(request.getParameter("qEnd"));
        	currentQues = Integer.parseInt(qCurrent);
        			
            if(currentQues<startQues) currentQues=startQues;
            if(currentQues>endQues) currentQues=endQues;
            
        } catch (Exception e) {
        }
        
        updateExamStausInSession(request);
        
        List<ExamStatus> qListForSave = new ArrayList<ExamStatus>();
        ExamStatusInfo  examStatusInfo =Utils.getExamStatusInSession(request);
        
        examStatusInfo.getExamInfo().forEach((String key,ExamInfo examInfo) -> {
        	ExamStatus status = new ExamStatus();
            status.setUsername(Utils.getloggedInUserName());
            status.setExamcode(examStatusInfo.getExamcode());
            status.setQuestionsnum(examInfo.getQuestionsNum());
            status.setSelectedoptions(Utils.getOptionAsString(examInfo.getSelOptionsList()));
            status.setMatched(examInfo.isMatched());
            status.setMarks(examInfo.getMarks());
            qListForSave.add(status);
        });
        
        questionService.saveExam(qListForSave);
 
		PaginationResult<OuestionsInfo> paginationResult = questionService.listOuestions(currentQues, MAX_RESULT,
				MAX_NAVIGATION_PAGE, Utils.getExamStatusInSession(request));
		
	     model.addAttribute("startQues", startQues);
	     model.addAttribute("endQues", endQues);
	     model.addAttribute("currentQues", currentQues);
	     model.addAttribute("paginationQuestionResult", paginationResult);
	     model.addAttribute("qList", examStatusInfo.getExamInfo());
		
        
        return "questionsList";
	}
	
	@PostMapping("/submitExam")
    public String submitExam(Model model, HttpSession session, HttpServletRequest request,
    		@RequestParam(value = "question", defaultValue = "1") String pageStr) {
		
		int page = 0;
        final int MAX_RESULT = 1;
        final int MAX_NAVIGATION_PAGE = 30;
        
        try {
            page = Integer.parseInt(pageStr);
            if(page==0) page=1;
            if(page>MAX_NAVIGATION_PAGE) page=MAX_NAVIGATION_PAGE;
        } catch (Exception e) {
        }
        
        updateExamStausInSession(request);
 
		PaginationResult<OuestionsInfo> paginationResult = questionService.listOuestions(page, MAX_RESULT,
				MAX_NAVIGATION_PAGE, Utils.getExamStatusInSession(request));
		model.addAttribute("paginationQuestionResult", paginationResult);
        
        
        return "questionsList";
	}
	
	
	//update exam status
	public void updateExamStausInSession(HttpServletRequest request) {
		
		final Logger logger;
		logger = LoggerFactory.getLogger(this.getClass());
		
		String selectedOptions="";
		List<String> selectedOptionsList=new ArrayList();
		
		String quenumber = (String)request.getParameter("quenumber");
		String queAns = (String)request.getParameter("queAns");
		String[] options = (String[])request.getParameterValues("quesOptions");
		
		logger.info("quenumber : "+quenumber);
		logger.info("queAns : "+queAns);
		
		if(queAns!=null) queAns=StringUtils.trimWhitespace(queAns);
		
		if(options!=null) {
			for(int i=0; i<options.length; i++){
				selectedOptions+=StringUtils.trimWhitespace(options[i]);
				selectedOptionsList.add(StringUtils.trimWhitespace(options[i]));
			}
		}
		String examcode =(String)request.getSession().getAttribute("examcode");
		
		logger.info("options : "+selectedOptions);
		logger.info("exam code in update status >>>>>> : "+examcode);
		ExamStatusInfo examStatusInfo = Utils.getExamStatusInSession(request);
		examStatusInfo.setExamcode(examcode);
		
		logger.info("options : "+selectedOptions);
		
		if(quenumber!=null) {
			ExamInfo info = new ExamInfo();
			info.setQuestionsNum(Integer.parseInt(quenumber));
			info.setSelOptionsList(selectedOptionsList);
			
			if(queAns!=null && queAns.equalsIgnoreCase(selectedOptions)) {
				info.setMatched(true);
				info.setMarks(1);
			}
			
			logger.info("Selected Option is Correct : "+info.isMatched());
			examStatusInfo.getExamInfo().put(quenumber,info);
		}
		
		request.getSession().setAttribute(examcode+"Data", examStatusInfo);
	}
}