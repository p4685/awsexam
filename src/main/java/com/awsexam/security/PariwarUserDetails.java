package com.awsexam.security;

import java.util.Collection;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import com.awsexam.model.User;

@SuppressWarnings("serial")
public class PariwarUserDetails implements UserDetails {
	private List<String> userRoles;
	private  transient User user;
	public PariwarUserDetails(User user, List<String> userRoles) {
		this.user = user;
		this.userRoles = userRoles;
	}
	
	public String getFullUsername() {
		return user.getName();
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		String roles = StringUtils.collectionToCommaDelimitedString(userRoles);
		return AuthorityUtils.commaSeparatedStringToAuthorityList(roles);
	}
	@Override
	public String getUsername() {
		if(user==null) return "";
		
		return user.getUserId();
	}
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	@Override
	public boolean isEnabled() {
		return true;
	}
	@Override
	public String getPassword() {
		return user.getPassword();
	}
}

