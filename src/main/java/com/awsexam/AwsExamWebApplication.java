package com.awsexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource(value = {"classpath:configuration.properties"})
public class AwsExamWebApplication {
	public static void main(String[] args) {
		SpringApplication.run(AwsExamWebApplication.class, args);
	}
}
