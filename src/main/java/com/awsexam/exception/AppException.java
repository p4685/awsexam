package com.awsexam.exception;
@SuppressWarnings("serial")
public class AppException extends Exception {
	public AppException(String message) {
		super(message);
	}
}