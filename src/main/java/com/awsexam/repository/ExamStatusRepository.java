package com.awsexam.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.awsexam.entity.ExamStatus;


@Transactional
@Repository
public interface ExamStatusRepository extends JpaRepository<ExamStatus, String>  {

  @Query("select e from ExamStatus e where e.username=:username and e.examcode=:examcode and e.questionsnum=:questionsnum")
  ExamStatus getExamStatus(String username,String examcode,int questionsnum);
  
  @Modifying(clearAutomatically = true)
  @Transactional
  @Query(value="update ExamStatus set selectedoptions=:selectedoptions,matched=:matched,marks=:marks where username=:username and examcode=:examcode and questionsnum=:questionsnum")
  int updateExam(String username,String examcode,int questionsnum,String selectedoptions,boolean matched,int marks);
  
  @Query("select e from ExamStatus e where e.username=:username and e.examcode=:examcode")
  List<ExamStatus> getAllQuestion(String username,String examcode);
  
  @Query("select e from ExamStatus e where e.username=:username")
  List<ExamStatus> getAllQuestion(String username);
  
}