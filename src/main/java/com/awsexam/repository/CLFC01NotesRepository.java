package com.awsexam.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.awsexam.entity.QuestionsNotes;

@Transactional
@Repository
public interface CLFC01NotesRepository extends JpaRepository<QuestionsNotes, String>  {

  @Query("select n.notes from QuestionsNotes n where n.queNumber.queNumber=:quenumber")
  String findNotes(@Param("quenumber") Integer quenumber);
  
}