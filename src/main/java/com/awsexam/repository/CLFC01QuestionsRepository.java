package com.awsexam.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.awsexam.entity.Question;

@Transactional
@Repository
public interface CLFC01QuestionsRepository extends JpaRepository<Question, String>  {

    
  @Query("select q from Question q where q.queNumber=:queNumber")
  Question findQuestions(@Param("queNumber") Integer queNumber);
  
  @Query("select q from Question q")
  List<Question> listQuestions();

  /*@Query("select new com.pariwarstore.model.OuestionsInfo(q.queNumber,q.que, q.ans) from Question")
  List<OuestionsInfo> listQuestions();*/
  
}