package com.awsexam.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.awsexam.entity.AwsExam;

@Transactional
@Repository
public interface AwsExamRepository extends JpaRepository<AwsExam, Integer>  {

  @Query("select e from AwsExam e")
  List<AwsExam> getAwsExams();
   
}