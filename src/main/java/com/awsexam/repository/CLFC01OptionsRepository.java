package com.awsexam.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.awsexam.entity.QuestionsOptions;
import com.awsexam.model.OptionsInfo;

@Transactional
@Repository
public interface CLFC01OptionsRepository extends JpaRepository<QuestionsOptions, String>  {
 
  @Query("select new com.awsexam.model.OptionsInfo(qOps.queNumber.queNumber,qOps.code, qOps.optionTxt) from QuestionsOptions qOps where qOps.queNumber.queNumber=:queNumber")
  List<OptionsInfo> listOptions(int queNumber);
  
}