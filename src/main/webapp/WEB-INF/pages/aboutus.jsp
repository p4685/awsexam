<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<!-- ABOUT US -->
        <div class="row marginbottom-150">
		<div class="container col-sm-offset-1 col-sm-5 col-left ">
			<h1 class="capital">
				<strong>About Us</strong>
			</h1>
			<span class="st-border"></span>
			<div class="about-us ">
				<br>
				<p>
					<b>Why Pariwar Super Shopee?<br></b> Pariwar Super Shopee is a low-price supermarket that allows you to buy products across categories 
					like grocery, vegetables, beauty & wellness, household care, baby care and variety of other products..
				</p>
			</div>
		</div>
	<!-- /ABOUT US -->