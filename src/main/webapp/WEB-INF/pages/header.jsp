<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- HEADER -->
<header id="header">
	<nav class="navbar st-navbar navbar-fixed-top" >
		 	<div class="collapse navbar-collapse" id="st-navbar-collapse">
			<div  style="width:95%;">
				<ul class="nav navbar-nav navbar-right margintop-5 " >
					<li>  <a class="btn btn-md " href="register" style="margin-top:10px;border-top-width:1px">
                       <span class="glyphicon glyphicon-user"></span>&nbsp;Sign Up
                    </a> </li>
			        <li><a class="btn btn-md  nav-links-right  " href="login" style="margin-top:10px;border-top-width:1px">
                       <span class="glyphicon glyphicon-log-in"></span>&nbsp;Login
                    </a></li> 
                 </ul> 
                 <ul class="nav navbar-nav nav-links">
					 <a class="logo1"  href="${pageContext.servletContext.contextPath}">
						<img id="a" style="padding:0px 0px;width:200px;vertical-align:top;" src="<c:url value='images/pariwar.png'/>"></img> 
					 </a>
				</ul>
				<hr style="width:100%;height:2px;border-width:1;color:gray;background-color:gray">
			</div>
		</div>  
	</nav>
</header>
<br>
<!-- /HEADER -->