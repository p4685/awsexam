<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Pariwar Super Shoppee</title>
<link rel="stylesheet" href="<c:url value='css/parivar.css'/>" type="text/css" media="all">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	    <jsp:include page="header.jsp"></jsp:include>
	    <!-- SIGNUP -->
		<div class="container col-right signup" style="margin-top:60px; width :60%">
			<div class="panel panel-default">
				<div class="panel-heading" style="color:#f5f5f5;background-color:#337ab7;border-color:#337ab7;">
					<h4 style="margin-top:40px;margin-bottom: 0px;" class="capital"><spring:message text="Register" /> </h4>
				</div>
				<div class="panel-body">
					<form:form method="POST" action="${pageContext.servletContext.contextPath}/registerUser" modelAttribute="command" >
					    <div class="row">
						<div class="form-group col-sm-offset-1 col-sm-4 text-right fontstyle" >
							<spring:message text="User Id"/> <span style="color: red">*</span>
							</div>
							<div class="col-sm-6">
								<form:input path="userId" class="form-control" />
								<br> 
								<form:errors path="userId" />
							</div>
							<br>
						</div>
						<div class="row">
						<div class="form-group col-sm-offset-1 col-sm-4 text-right fontstyle" >
							<spring:message  text="Password" />
							<span style="color: red">*</span>
							</div>
							<div class="col-sm-6">
								<form:password path="password" class="form-control" />
								<br> 
								<form:errors path="password" />
							</div>
							<br>
						</div>
						 <div class="row">
						<div class="form-group col-sm-offset-1 col-sm-4 text-right fontstyle" >
							<spring:message 
								text="User Name" />
							<span style="color: red">*</span>
							</div>
							<div class="col-sm-6">
								<form:input path="name" class="form-control" />
								<br> <form:errors path="name" />
							</div>
							<br>
						</div>
						 <div class="row">
						<div class="form-group col-sm-offset-1 col-sm-4 text-right fontstyle" >
							<spring:message 
								text="City" />
							<span style="color: red">*</span>
							</div>
							<div class="col-sm-6">
								<form:input path="city" class="form-control" />
								<br> 
								<form:errors path="city" />
							</div>
							<br>
						</div>
						<div class="row">
						<div class="form-group col-sm-offset-1 col-sm-4 text-right fontstyle" >
							<spring:message 
								 text="Email Id" />
							<span style="color: red">*</span>
							</div>
							<div class="col-sm-6">
								<form:input path="email" class="form-control" />
								<br> 
								<form:errors path="email" />
							</div>
							<br>
						</div>
						 <div class="row">
						<div class="form-group col-sm-offset-1 col-sm-4 text-right fontstyle" >
							<spring:message 
							text="Phone Number" />
							<span style="color: red">*</span>
						</div>
							<div class="col-sm-6">
								<form:input path="phone" class="form-control" maxlength="10" />
								<br> 
								<form:errors path="phone" />
							</div>
							<br>
						</div>
						 <div class="row">
						<div class="form-group col-sm-offset-1 col-sm-3 text-right fontstyle" >
						</div>
							<div class="col-sm-6">
								<input type="submit" class="btn btn-primary"
									value="<spring:message text="Register"/>"/>
							</div>
							<div class="col-md-12 text-center">
			            <div class="text-center text-danger">${message}</div>  
			            </div></div><br>
			            <c:if test="${successMessage ne null}">
							<div class="alert alert-success col-md-12" align="center">${successMessage}
							<br> Click <a href="login" >  &nbsp;here </a>to login.
			                  </div>
						</c:if>		
			</form:form>
			</div>
		</div>
		</div>
		</div>
	<!-- /SIGNUP -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>