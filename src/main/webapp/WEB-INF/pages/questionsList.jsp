<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Question</title>
<link rel="stylesheet" href="<c:url value='css/parivar.css'/>" type="text/css" media="all">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<!-- HEADER -->
	<fmt:setLocale value="en_IN" scope="session"/>
	<header id="header">
		<nav class="navbar st-navbar navbar-fixed-top">
			<div class="container-fluid" align="center">
				 <div class="collapse navbar-collapse" id="st-navbar-collapse">
				 <div style="width:95%;margin-left:10px;align:center">
						<ul class="nav navbar-nav navbar-right margintop-5" >
							<li class="btn btn-md fontstyle"  style="margin-top:5px;border-top-width:1px;color:SteelBlue"> 
							<span class="glyphicon glyphicon-user"></span>Welcome <b>${userName}</b>&nbsp;</li>
							<li> <a class="btn btn-md " href="listExams" style="color:green;margin-top:10px;border-top-width:1px">
								<span class="glyphicon glyphicon-log-out"></span>&nbsp;Exam List
							</a> </li>
							<li> <a class="btn btn-md " href="logout" style="color:green;margin-top:10px;border-top-width:1px">
								<span class="glyphicon glyphicon-log-out"></span>&nbsp;Logout
							</a> </li>
						</ul>  
						<ul class="nav navbar-nav nav-links">
						 <a class="logo1"  href="${pageContext.servletContext.contextPath}">
							<img id="a" style="padding:0px 0px;width:200px;height:100px;vertical-align:top;" src="<c:url value='images/cloud.png'/>"></img> 
						 </a>
						</ul>
						<hr style="width:100%;height:2px;border-width:1;color:gray;background-color:gray"/>
				 </div>
				</div>  
			</div>
		</nav>
    </header>
	<!-- /HEADER -->
	  <div class="container col-sm-5 col-right" align="left" style="width:95%;height:100%;margin-top:150px;margin-left:45px;">
	 		<div class="panel panel-default">
		         <div style="color:#f5f5f5;background-color:#337ab7;border-color:#337ab7;">   
		           <span></span>&nbsp;<strong>AWS Exam - (CLFC01) </strong>
				 </div>
				 <div style="color:#f5f5f5;background-color:white;border-color:#337ab7;margin-top:15px;margin-left:25px;">   
		           <c:if test="${paginationQuestionResult.totalPages > 1}">
				            <c:forEach begin="${startQues}" end="${endQues}" var="index">
				               <c:set var="qstatus" value="false" />
				               <c:if test="${qList!=null}">
				                   <c:if test="${qList.get(String.valueOf(index)) !=null}">
				                      <c:if test="${qList.get(String.valueOf(index)).selOptionsList.size()>0}">
				                         <c:set var="qstatus" value="true" />
				                      </c:if> 
				                   </c:if> 
				               </c:if>
				               <c:if test="${qstatus}">
				                  <c:if test="${qList.get(String.valueOf(index)).isMatched()}">
				                      <a href="#" class="nav-item" onclick="onQuestionClick(${index})">
										<label style="color:#f5f5f5;width:40px;background-color:green; border-top :solid 3px transparent"><strong>${index}</strong></label>
									  </a>
				                  </c:if>
				                  <c:if test="${!qList.get(String.valueOf(index)).isMatched()}">
				                     <a href="#" class="nav-item" onclick="onQuestionClick(${index})">
										<label style="color:#f5f5f5;width:40px;background-color:brown; border-top :solid 3px transparent"><strong>${index}</strong></label>
									  </a>
				                  </c:if>
				               </c:if>
				               <c:if test="${!qstatus}">
				                 <a href="#" class="nav-item" onclick="onQuestionClick(${index})">
									<label style="color:#f5f5f5;width:40px;background-color:#337ab7; border-top :solid 3px transparent"><strong>${index}</strong></label>
								  </a>
				               </c:if>
							</c:forEach>
					</c:if>
				 </div>
			 	 <div style="width:95%;margin-top:15px;margin-left:25px;">
			 	 	    <form:form method="POST" id="questionForm" modelAttribute="questionForm" action="${pageContext.request.contextPath}/listQuestion">
						   <table border="1" style="width:100%;font-size:14px;border-color:black">
						       <tr style="color:#f5f5f5;background-color:#337ab7;border-color:#337ab7;padding: 0px 0px;">
						           <th style="padding: 5px 15px;">Number</th>
						           <th style="padding: 5px 15px;">Question</th>
						       </tr>
						       <input type="textbox" hidden='true' id="qStart" name="qStart" value='${startQues}'/>
						       <input type="textbox" hidden='true' id="qEnd" name="qEnd" value='${endQues}'/>
						       <c:forEach items="${paginationQuestionResult.list}" var="questionInfo">
						           <tr style="padding: 5px ">
						               <td style="padding: 5px 15px;width:5%;">${questionInfo.queNumber}
						               <input type="textbox" hidden='true' id="quenumber" name="quenumber" value='${questionInfo.queNumber}'/>
						               <input type="textbox" hidden='true' id="queAns" name="queAns" value='${questionInfo.ans}'/>
						               </td>
						               <td style="padding: 5px 15px;width:95%;">${questionInfo.que}
						               <a href="#" title="${questionInfo.ans}" style="color:#00CC00;"><span class="glyphicon glyphicon-flag"></span>&nbsp;</a></a>
						               <a href="#" title="${questionInfo.notes}" style="color:#00CC00;"><span class="glyphicon glyphicon-flag"></span>&nbsp;</a></a>
						               </td>
						           </tr>
						           <c:forEach items="${questionInfo.options}" var="options">
							           <tr style="padding: 5px 15px;">
							               <td style="padding: 5px 15px;width:10%;">
							               <input type="checkbox" id="quesOptions" ${options.checked} name="quesOptions" value='${options.code}'/>
							               ${options.code}</td>
							               <td style="padding: 5px 15px;width:80%;">${options.optionTxt}</td>
							           </tr>
						       	   </c:forEach>
						       </c:forEach>
						   </table>
						   <br>
						    <c:if test="${paginationQuestionResult.totalPages > 1}">
						       <div class="page-navigator" >
						          <a href="#" onClick="onQuestionFormSubmit(${paginationQuestionResult.getCurrentPage()+1})" class="nav-item">
				                  <span class="glyphicon glyphicon-hand-right"></span>&nbsp;<strong>Next</strong></a> 
				                  </a>&nbsp;
				                  <a href="#" onClick="onQuestionFormSubmit(${paginationQuestionResult.getCurrentPage()-1})" class="nav-item">
						           <span  class="glyphicon glyphicon-hand-left"></span>&nbsp;<strong>Back</strong></a>
						          </a>&nbsp;
						          <a href="#" onClick="saveExam(${paginationQuestionResult.getCurrentPage()-1})" class="nav-item">
						           <span class="glyphicon glyphicon-save"></span>&nbsp;<strong>Save</strong></a>
						          </a>&nbsp;
						           <a href="#" onClick="saveExam(${paginationQuestionResult.getCurrentPage()-1})" class="nav-item">
						           <span class="glyphicon glyphicon-save"></span>&nbsp;<strong>Submit</strong></a>
						          </a>
						       </div>
						   </c:if>
						</form:form>   
	  	 		</div>
			</div>
		</div>
<script>
  function onQuestionFormSubmit(currentQues){
	  var questionForm = document.getElementById('questionForm');
	  questionForm.action='navigateQuestion?qCurrent='+currentQues;
	  questionForm.submit();
  }
  function saveExam(currentQues){
	  var questionForm = document.getElementById('questionForm');
	  questionForm.action='saveExam?qCurrent='+currentQues;
	  questionForm.submit();
  }
  function onQuestionClick(currentQues){
	  var questionForm = document.getElementById('questionForm');
	  questionForm.action='navigateQuestion?qCurrent='+currentQues;
	  questionForm.submit();
  }
</script>
</body>
</html>