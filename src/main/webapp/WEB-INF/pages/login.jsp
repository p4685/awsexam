<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>AWS Exam</title>
<link rel="stylesheet" href="<c:url value='css/parivar.css'/>" type="text/css" media="all">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<!-- HEADER -->
	<header id="header">
		<nav class="navbar st-navbar navbar-fixed-top" >
			 	<div class="collapse navbar-collapse" id="st-navbar-collapse">
				<div style="width:90%;margin-left:50px;" >
					<ul class="nav navbar-nav navbar-right margintop-5 " >
						<li>  <a class="btn btn-md " href="register" style="margin-top:10px;border-top-width:1px">
	                       <span class="glyphicon glyphicon-user"></span>&nbsp;Sign Up
	                    </a> </li>
				        <li><a class="btn btn-md  nav-links-right  " href="login" style="margin-top:10px;border-top-width:1px">
	                       <span class="glyphicon glyphicon-log-in"></span>&nbsp;Login
	                    </a></li> 
	                 </ul> 
	                 <ul class="nav navbar-nav nav-links">
						 <a class="logo1"  href="${pageContext.servletContext.contextPath}">
							<img id="a" style="padding:0px 0px;width:200px;vertical-align:top;" src="<c:url value='images/cloud.png'/>"></img> 
						 </a>
					</ul>
					<hr style="width:100%;height:2px;border-width:1;color:gray;background-color:gray">
				</div>
			</div>  
		</nav>
	</header>
   <!-- /HEADER -->
   <br><br><br>
	<!-- Login start -->
		<div class="container  col-right " style="width:40%;float:center;">
			<br> <br> <br>
			   <div class="panel panel-default">
				<div class="panel-heading" style="margin-top:15px;color:#f5f5f5;background-color:#337ab7;border-color:#337ab7;padding:0px 0px">
					<h4 class="capital">Login</h4>
				</div>
				<div class="panel-body">
					<h3 class="red" id="msg2"></h3>
						<form id="login" action="${pageContext.servletContext.contextPath}/authenticateLogin" method='POST'>
							<div class="input-group">  
					            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>  
					            <input type="text" class="form-control" placeholder="Enter Your name" id="userName" name="userName" required>
					        </div>  
						    <div class="input-group">  
					            <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span> 
					            <input type="password" class="form-control" placeholder="Enter Password" id="password" name="password" required> 
					        </div>  
							<div class="col-sm-offset-4 col-sm-10 text-left"> 
							<button type="submit" id="loginbtn" class="btn btn-primary">Login</button></div><br><br>
							 <input type="hidden"  name="${_csrf.parameterName}"  value="${_csrf.token}"/>
					   			<c:if test="${param.error ne null}">
						            <div class="col-md-12 text-center">
						            <div class="text-center text-danger">Invalid credentials!</div>  
									<li><a class="btn btn-md  nav-links-right  " href="forgotPassword" style="margin-top:10px;border-top-width:1px">
				                       <span class="glyphicon glyphicon-log-in"></span>&nbsp;Forgot password click here to reset
				                    </a></li> 
					            </div>
					            </c:if>       
						</form>
					</div>
				</div>
			</div>
	</div>
	<!-- Login end -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
