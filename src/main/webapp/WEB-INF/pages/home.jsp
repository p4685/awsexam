<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta>
<title>Pariwar Super Shopee</title>
<!-- Main CSS file -->
<link rel="stylesheet" href="<c:url value='/css/parivar.css'/>" type="text/css" />

<!-- Favicon -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body data-spy="scroll" data-target="#footer">
	<!-- HEADER -->
	<header id="header">
		<nav class="st-navbar navbar-fixed-top">
			<div class="collapse navbar-collapse" id="st-navbar-collapse"  style="width:95%;margin-left:50px;">
				<div>
					<ul class="nav navbar-nav navbar-right margintop-5 ">
						<li><a class="btn btn-md " href="register"
							style="margin-top: 10px; border-top-width: 1px"> <span
								class="glyphicon glyphicon-user"></span>&nbsp;Sign Up
						</a></li>
						<li><a class="btn btn-md  nav-links-right  " href="login"
							style="margin-top: 10px; border-top-width: 1px"> <span
								class="glyphicon glyphicon-log-in"></span>&nbsp;Login
						</a></li>
					</ul>
					<ul class="nav navbar-nav nav-links">
						<a class="logo1" href="${pageContext.servletContext.contextPath}">
							<img id="a"
							style="padding: 0px 0px; width: 200px; vertical-align: top;"
							src="<c:url value='images/pariwar.png'/>"></img>
						</a>
					</ul>
					<hr style="width: 100%; height: 2px; border-width: 1; color: gray; background-color: gray">
				</div>
			</div>
		</nav>
	</header>
	<!-- /HEADER -->
	<br>
	<br>
	<br>
	<br>
	<br>
	<!-- CONTACT -->
	<section id="contact" style="background-color: #ffff">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h1 class="capital">
							<strong>Contact us</strong>
						</h1>
						<span class="st-border"></span>
					</div>
				</div>
				<div class="col-sm-4 contact-info">
					<br>
					<p class="st-address">
						<i class="fa fa-map-marker"></i> Pariwar Super shoppe , Indori,Pin
						- 422601
					</p>
					<p class="st-phone">
						<i class="fa fa-mobile"></i> 9860088630
					</p>
					<p class="st-email">
						<i class="fa fa-envelope-o"></i> parigha.nawale@gmail.com
					</p>
					<p class="st-website">
						<i class="fa fa-globe"></i> www.pariwarshopee.com
					</p>
				</div>
				<div class="col-sm-7 col-sm-offset-1">
					<form id="contactForm" class="contact-form" name="contact-form"
						method="post">
						<div class="row">
							<div class="col-sm-6">
								<input type="text" name="name" required="required" placeholder="Name*">
							</div>
							<div class="col-sm-6">
								<input type="email" name="email" required="required" placeholder="Email*">
							</div>
							<div class="col-sm-6">
								<input type="text" name="mobile" required="required" placeholder="Mobile*" pattern="[789][0-9]{9}">
							</div>
							<div class="col-sm-6">
								<datalist id="fdlist">
									<option>Feedback</option>
									<option>Complaint</option>
								</datalist>
								<input type="text" list="fdlist" name="casestype"
									placeholder="Case type:feedback/complaint*" required>
							</div>
							<div class="col-sm-12">
								<textarea name="message" required="required" cols="30" rows="7"
									placeholder="Message*"></textarea>
							</div>
							<div class="col-sm-12">
								<input type="submit" name="submit" value="Send Message"
									class="btn btn-send" />
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- /CONTACT -->

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
	<script>
		var $contactForm = $('#contactForm');
		$contactForm
				.on(
						'submit',
						function(event) {
							event.preventDefault();
							bootbox
									.alert("Thanks for contacting us!! Our team will connect to you soon.");
						});
	</script>
</body>
</html>