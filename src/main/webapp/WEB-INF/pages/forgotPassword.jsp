
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>InfyGo Reservation System</title>

<link rel="stylesheet" href="<c:url value='css/newstyle.css'/>"
type="text/css" media="all">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" 
rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<c:url value='/resources/css/custom.css'/>" type="text/css" media="all">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<!-- HEADER -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- /HEADER -->
	<!-- Login start -->
    <!-- ABOUT US -->
    <jsp:include page="aboutus.jsp"></jsp:include>
	<!-- /ABOUT US -->
	
	<div class="container  col-sm-5 col-right " style="width:50%">
			<br> <br> <br>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="capital">Forgot Password</h4>
				</div>
				<div class="panel-body">
					<h3 class="red" id="msg2"></h3>
					<form:form method="POST" action="resetPassword" modelAttribute="command">
					<!-- <h7><font class="control-label col-sm-offset-1 col-sm-3 text-right fontstyle">Email&Phone Provided during registration with InfyGO<span
										class="red">*</span>
								</font></h7> -->
							<div class="form-group">
								<form:label path="email"
									class="control-label col-sm-offset-1 col-sm-3 text-right fontstyle">Email Id<span
										class="red">*</span>
								</form:label>
								<div class="col-sm-7">
									<form:input path="email" class="form-control" />
									<br>
									<form:errors path="email" />
								</div>
								<br>
							</div>
							<div class="form-group">
								<form:label path="phone"
									class="control-label col-sm-offset-1 col-sm-3 text-right fontstyle">Phone Number<span
										class="red">*</span>
								</form:label>
								<div class="col-sm-7">
									<form:input path="phone" class="form-control" />
									<br>
									<form:errors path="phone" />
								</div>
								<br>
							</div>
							<div class="form-group">
								<form:label path="newPassword"
									class="control-label col-sm-offset-1 col-sm-3 text-right fontstyle">New Password<span
										class="red">*</span>
								</form:label>
								<div class="col-sm-7">
									<form:input type="password" path="newPassword"
										class="form-control" />
									<br>
									<form:errors path="newPassword" />
								</div>
								<br>
							</div>
							<br><br><br><br>
							<div class="form-group">
								<div class="col-sm-offset-1">
									<font class="control-label text-right fontstyle">Please enter your Email Id and Phone number for confirmation</font>
								</div>
							</div>
						  <div class="form-group">
								<div class="col-sm-offset-4 col-sm-7">
									<button
										type="submit" class="btn btn-primary fontfamily">Reset Password</button>
								</div>
							</div>
					</form:form>
					<div class="col-md-12 text-center">
                   <div class="text-center text-danger">${message}</div>  
                   </div>
					  <c:if test="${successMessage ne null}">
						<div class="alert alert-success col-md-12" align="center">${successMessage}
						<br>
						Click <a href="login" > &nbsp;here </a>to login.
                  		</div>
					</c:if>
					</div>
				</div>
			</div>
	</div>
	<!-- Login end -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>

