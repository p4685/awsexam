<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Exam List</title>
<link rel="stylesheet" href="<c:url value='css/parivar.css'/>" type="text/css" media="all">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<!-- HEADER -->
	<fmt:setLocale value="en_IN" scope="session"/>
	<!-- /HEADER -->
	  
	<header id="header">
	<nav class="navbar st-navbar navbar-fixed-top" >
		<div class="container-fluid">
		 <div class="collapse navbar-collapse" id="st-navbar-collapse">
		 <div style="width:95%;margin-left:50px;">
				<ul class="nav navbar-nav navbar-right margintop-5" >
			       	<li class="btn btn-md fontstyle"  style="margin-top:5px;border-top-width:1px;color:SteelBlue"> 
			       	<span class="glyphicon glyphicon-user"></span>Welcome <b>${userName}</b>&nbsp;</li>
			       	<li> <a class="btn btn-md " href="listExams" style="color:green;margin-top:10px;border-top-width:1px">
                      	<span class="glyphicon glyphicon-log-out"></span>&nbsp;Exam List
                	</a> </li>
					<li> <a class="btn btn-md " href="logout" style="color:green;margin-top:10px;border-top-width:1px">
                      	<span class="glyphicon glyphicon-log-out"></span>&nbsp;Logout
                	</a> </li>
				</ul>  
				<ul class="nav navbar-nav nav-links">
				 <a class="logo1"  href="${pageContext.servletContext.contextPath}">
					<img id="a" style="padding:0px 0px;width:200px;vertical-align:top;" src="<c:url value='images/cloud.png'/>"></img> 
				 </a>
			    </ul>
				<hr style="width:100%;height:2px;border-width:1;color:gray;background-color:gray">
		 </div>
		</div>  
	</div>
	</nav>
    </header>
	<!-- /HEADER --><br><br><br>
	   <div class="container col-sm-5 col-right" style="align:center;width:95%;padding:0px 20px;margin-left:40px;height:50%">
	 		<div class="panel panel-default">
		         <div style="color:#f5f5f5;background-color:#337ab7;border-color:#337ab7;padding:25px 5px;width:100%;height:50%">   
		           <span></span>&nbsp;<strong>AWS Exam - (CLFC01) </strong>
				 </div>
			 	 <div style="width:100%;margin-left:30px;margin-top:50px;">
			 	 	<div class="customer-info-container">
						   <table border="1" style="width:95%;font-size:12px;border-color:black">
						       <tr align="center" style="color:#f5f5f5;background-color:#337ab7;border-color:black;">
						           <th style="padding-left:5px">Number</th>
						           <th style="padding-left:25px">Name</th>
						           <th style="padding-left:5px">Total Questions</th>
						           <th style="padding-left:5px">Attended Questions</th>
						           <th style="padding-left:5px">Total Marks</th>
						           <th style="padding-left:5px">Percentage</th>
						           <th style="padding-left:5px">Result</th>
						    	   <th style="padding-left:5px">Action</th>
						       </tr>
						       <c:forEach items="${awsExamList}" var="exam" varStatus="count">
						           <c:set var="bColor" value="#C0C0C0" />
						           <c:if test="${exam.started}">
						       			<c:set var="bColor" value="#ffc252" />
						           </c:if>
						           <tr align="center" style="color:black;background-color:${bColor};border-color:black;">
						           	   <td style="align:center;padding-left:5px"><strong>${count.index+1}</strong></td>
						               <td><strong>${exam.name}</strong></td>
						               <td><strong>50</strong></td>
						               <td><strong>${exam.attended}</strong></td>
						               <td><strong>${exam.totalMarks}</strong></td>
						               <td><strong>${exam.percentage}</strong></td>
						               <td><strong>${exam.result}</strong></td>
						               <td>
						                <c:if test="${exam.started}">
						                	<a class="btn btn-md " href="getQuestions?qStart=${exam.qstart}&qCurrent=${exam.qstart}&qEnd=${exam.qend}&examcode=${exam.code}" 
							               		style="color:green;border-top-width:1px">
						                      	<span class="glyphicon glyphicon-check"></span>&nbsp;<strong>Resume</strong>
					                		</a>
						                </c:if>
					               		<c:if test="${!exam.started}">
					               			<a class="btn btn-md " href="getQuestions?qStart=${exam.qstart}&qCurrent=${exam.qstart}&qEnd=${exam.qend}&examcode=${exam.code}" 
							               		style="color:green;border-top-width:1px">
						                      	<span class="glyphicon glyphicon-check"></span>&nbsp;<strong>Start</strong>
					                		</a>
					               		</c:if>
                					</td>
						           </tr>
						       </c:forEach>
						   </table>
		 			</div>
	  	 		</div>
			</div>
		</div>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
