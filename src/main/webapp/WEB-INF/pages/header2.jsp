<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- HEADER -->
<header id="header">
	<nav class="navbar st-navbar navbar-fixed-top" >
		<div class="container-fluid">
		 <div class="collapse navbar-collapse" id="st-navbar-collapse">
		 <div style="width:95%;margin-left:50px;">
				<ul class="nav navbar-nav navbar-right margintop-5" >
			       	<li class="btn btn-md fontstyle"  style="margin-top:5px;border-top-width:1px;color:SteelBlue"> 
			       	<span class="glyphicon glyphicon-user"></span>Welcome <b>${userName}</b>&nbsp;</li>
					<li> <a class="btn btn-md " href="orderSearch" style="color:green;margin-top:10px;border-top-width:1px">
                      	<span class="glyphicon glyphicon-search"></span>&nbsp;Search Order
                	</a> </li>
                	<li> <a class="btn btn-md " href="orderList" style="color:green;margin-top:10px;border-top-width:1px">
                      	<span class="glyphicon glyphicon-check"></span>&nbsp;Order List
                	</a> </li>
					<li> <a class="btn btn-md " href="product" style="color:green;margin-top:10px;border-top-width:1px">
                      	<span class="glyphicon glyphicon-check"></span>&nbsp;Product
                	</a> </li>
                	<li> <a class="btn btn-md " href="shoppingCart" style="color:green;margin-top:10px;border-top-width:1px">
                      	<span class="glyphicon glyphicon-check"></span>&nbsp;My Cart
                	</a> </li>
                	<li> <a class="btn btn-md " href="listQuestion?examcode=1to30" style="color:green;margin-top:10px;border-top-width:1px">
                      	<span class="glyphicon glyphicon-check"></span>&nbsp;Question List
                	</a> </li>
					<li> <a class="btn btn-md " href="logout" style="color:green;margin-top:10px;border-top-width:1px">
                      	<span class="glyphicon glyphicon-log-out"></span>&nbsp;Logout
                	</a> </li>
				</ul>  
				<ul class="nav navbar-nav nav-links">
				 <a class="logo1"  href="${pageContext.servletContext.contextPath}">
					<img id="a" style="padding:0px 0px;width:200px;vertical-align:top;" src="<c:url value='images/cloud.png'/>"></img> 
				 </a>
			    </ul>
				<hr style="width:100%;height:2px;border-width:1;color:gray;background-color:gray">
		 </div>
		</div>  
	</div>
	</nav>
    </header>
	<!-- /HEADER --><br><br><br>