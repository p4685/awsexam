<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- FOOTER -->
<footer id="footer" style="position:fixed;bottom:0px;width:100%;padding:1px;background-color:#286090">
<div class="container">
	    <div class="row">
		<div style="padding: 0px;background-color:#286090" class="col-sm-6 col-sm-push-6 footer-social-icons">
			<span>Follow us:</span>
			<a href=""><img src="<c:url value='images/fb.png'/>"></img></a>
			<a href=""><img src="<c:url value='images/gp.png'/>"></img></a>
			<a href=""><img src="<c:url value='images/yt.png'/>"></img></a>
			<a href=""><img src="<c:url value='images/t.png'/>"></img></a>
		</div>
		<!-- /SOCIAL ICONS -->
		<div class="col-sm-6 col-sm-pull-6" style="padding: 0px ">
			<p><COPYRIGHT/> 2020 <a href="${pageContext.servletContext.contextPath}">Pariwar Super Shopee</a>. All Rights Reserved.</p>
			</div>
			</div>
	</div>
</footer>
