FROM openjdk:8
ADD target/awsexam.war awsexam.war 
EXPOSE 8080
ENTRYPOINT ["java", "-jar" ,"awsexam.war"]
