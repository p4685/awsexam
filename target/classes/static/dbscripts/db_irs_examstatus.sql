-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_irs
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `examstatus`
--

DROP TABLE IF EXISTS `examstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `examstatus` (
  `username` varchar(30) NOT NULL,
  `questionsNum` int NOT NULL,
  `selectedoptions` varchar(30) NOT NULL,
  `matched` tinyint(1) DEFAULT NULL,
  `marks` int NOT NULL,
  `examcode` varchar(30) NOT NULL,
  UNIQUE KEY `Id` (`username`,`examcode`,`questionsNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `examstatus`
--

LOCK TABLES `examstatus` WRITE;
/*!40000 ALTER TABLE `examstatus` DISABLE KEYS */;
INSERT INTO `examstatus` VALUES ('aarush',101,'E',1,1,'101to150'),('aarush',102,'B',1,1,'101to150'),('aarush',120,'A',0,1,'101to150'),('aarush',150,'B',1,1,'101to150'),('aarush',1,'D',1,1,'1to50'),('aarush',2,'C',1,1,'1to50'),('aarush',3,'C',1,1,'1to50'),('aarush',4,'C',1,1,'1to50'),('aarush',5,'',0,0,'1to50'),('aarush',51,'AC',1,1,'51to100'),('aarush',64,'B',0,1,'51to100'),('aarush',100,'C',1,1,'51to100');
/*!40000 ALTER TABLE `examstatus` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-19 10:30:33
